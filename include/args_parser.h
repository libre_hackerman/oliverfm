/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 * Copyright (C) 2021 Abdon Senén Cruces Pérez <JustMeGunter@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ARGS_PARSER_H
#define ARGS_PARSER_H

#include <stdbool.h>

#define DEF_DELAY_DOWNLOAD 200  /* milliseconds */

struct args_s
{
	char *csv;
	bool keep_htmls;
	int delay_download;
	char first_page[2];
	char last_page[2];
};

bool
parse_arguments(int argc, char *argv[], struct args_s *args);

#endif
