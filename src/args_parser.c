/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 * Copyright (C) 2021 Abdon Senén Cruces Pérez <JustMeGunter@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include <args_parser.h>

/*
 * Displays the help, what did you expect?
 */

static void
display_help(const char *exec_name)
{
	int opt_width = 31;

	printf("Usage: %s [-h] [-k] [-d DELAY] [-f PAGE] [-l PAGE] CSV\n",
			exec_name);
	puts("\nScraps the dxmaps.com page and export a CSV file with the Callsign"
			" and Locator fields of the stations registered on the site.");
	puts("\nOptions:");
	printf("%-*sDon't delete the downloaded HTMLs after finishing\n",
			opt_width, "-k, --keep-htmls");
	printf("%-*sMilliseconds to wait before downloading a HTML to be nice with"
			" the server (def: %d)\n", opt_width, "-d, --delay-downloads <ms>",
			DEF_DELAY_DOWNLOAD);
	printf("%-*sSets the first page to start parsing from (def: AA)\n",
			opt_width, "-f, --first-page <page>");
	printf("%-*sSets the last page to parse from (def: ZZ)\n",
			opt_width, "-l, --last-page <page>");
	printf("%-*sDisplay this help\n", opt_width, "-h, --help");
}

/*
 * This function sets a struct args_s fields to its default values.
 */

static void
init_arguments(struct args_s *args)
{
	args->csv = NULL;
	args->keep_htmls = false;
	args->delay_download = DEF_DELAY_DOWNLOAD;
	memcpy(args->first_page, "AA", 2);
	memcpy(args->last_page, "ZZ", 2);
}

/*
 * This function receives the CLI arguments and a pointer to a struct args_s
 * and fills it with the data parsed from the CLI arguments. Returns true
 * if success.
 */

bool
parse_arguments(int argc, char *argv[], struct args_s *args)
{
	int op;
	struct option long_options[] = {
		{"help", no_argument, NULL, 'h'},
		{"keep-htmls", no_argument, NULL, 'k'},
		{"delay-downloads", required_argument, NULL, 'd'},
		{"first-page", required_argument, NULL, 'f'},
		{"last-page", required_argument, NULL, 'l'},
		{0, 0, 0, 0}
	};

	init_arguments(args);
	while ((op = getopt_long(argc, argv, ":hkd:f:l:", long_options, NULL)) != -1)
	{
		switch (op)
		{
			case 'h':
				display_help(argv[0]);
				exit(0);
				break;
			case 'k':
				args->keep_htmls = true;
				break;
			case 'd':
				args->delay_download = atoi(optarg);
				break;
			case 'f':
				if (strlen(optarg) != 2 ||
						!isupper(optarg[0]) || !isupper(optarg[1]))
				{
					fprintf(stderr, "%s is not a valid page\n", optarg);
					return (false);
				}
				memcpy(args->first_page, optarg, 2);
				break;
			case 'l':
				if (strlen(optarg) != 2 ||
						!isupper(optarg[0]) || !isupper(optarg[1]))
				{
					fprintf(stderr, "%s is not a valid page\n", optarg);
					return (false);
				}
				memcpy(args->last_page, optarg, 2);
				break;
			case ':':
				fputs("Missing required argument\n", stderr);
				return (false);
			case '?':
				fputs("Unknown CLI argument\n", stderr);
				return (false);
		}
	}

	if (optind == argc)
	{
		fputs("Missing CSV filepath\n", stderr);
		return (false);
	}
	args->csv = argv[optind];

	return (true);
}
