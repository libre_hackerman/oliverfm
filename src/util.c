/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 * Copyright (C) 2021 Abdon Senén Cruces Pérez <JustMeGunter@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <time.h>
#include <stdio.h>

#include <util.h>

/*
 * This function stops the thread for the amount of milliseconds passed.
 */

void
take_it_easy(unsigned long long millies)
{
	struct timespec ts;

	ts.tv_sec = millies / 1000000;
	ts.tv_nsec = millies % 1000000;

	nanosleep(&ts, NULL);
}

/*
 * This functions displays the time since the first call (at the beggining of
 * the program) and then passes the arguments to a printf to display a message.
 * It displays a newline at the end.
 */

void
log_message(const char *format, ...)
{
	static time_t time_start = 0;
	time_t time_diff;
	int hours, minutes, seconds;
	va_list ap;

	if (time_start == 0)
		time_start = time(NULL);

	time_diff = time(NULL) - time_start;
	hours = time_diff / 3600;
	time_diff %= 3600;
	minutes = time_diff / 60;
	time_diff %= 60;
	seconds = time_diff;

	printf("%.2d:%.2d:%.2d - ", hours, minutes, seconds);

	va_start(ap, format);
	vprintf(format, ap);
	va_end(ap);
	puts("");
	fflush(stdout);
}

/*
 * This function returns the size of a given file in MB.
 */

double
measure_filesize(const char *filepath)
{
	FILE *f;
	long bytes;

	if (!(f = fopen(filepath, "r")))
	{
		perror(filepath);
		return (0);
	}
	fseek(f, 0L, SEEK_END);
	bytes = ftell(f);
	fclose(f);

	return (bytes * 1.0 / 1024 / 1024);
}
