/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 * Copyright (C) 2021 Abdon Senén Cruces Pérez <JustMeGunter@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <libxml/HTMLparser.h>

#include <util.h>
#include <download_html.h>
#include <args_parser.h>

/*
 * This function dives into the mysterious waters of the HTML nodes until it
 * finds the <tr> corresponding to the first row of the table.
 * Return NULL if there's no table.
 */

static xmlNode *
get_first_row(xmlNode *root_element)
{
	xmlNode *cur;
	bool div_found;

	/* Sets to <body> childrens */
	cur = root_element->children->next->children;

	/* Iterate siblings until it reaches <h1> */
	while (strcmp((char*)cur->name, "h1") != 0)
		cur = cur->next;

	/* Iterate siblings until it reaches <div>. If there's no <div>, then
	 * the page doesn't have rows */
	div_found = false;
	while (cur)
	{
		if (strcmp((char*)cur->name, "div") == 0)
		{
			div_found = true;
			break;
		}
		cur = cur->next;
	}
	if (!div_found)
		return (NULL);

	/* Dive in childrens until it reaches <tr> */
	while (strcmp((char*)cur->name, "tr") != 0)
		cur = cur->children;
	/* Dive in childrens until it reaches <tr> */
	do
		cur = cur->children;
	while (strcmp((char*)cur->name, "tr") != 0);
	/* Skip header */
	cur = cur->next;

	return (cur);
}

/*
 * This function receives the index of a HTML page and writes to the passed
 * FILE the callsings and locators that it finds. It also increments the total
 * download counter and the total stations counter. Returns true on success.
 */

static bool
parse_table(const char *index, FILE *exported_file, struct args_s *args,
		double *total_downloaded, long *total_stations)
{
	char *filepath_html;
	htmlDocPtr doc;
	xmlNode *root_element, *row;
	char *callsign, *locator;
	long stations;

	if (!(filepath_html = download_html(index, total_downloaded, args)))
		return (false);

	log_message("Processing page %s...", index);
	doc = htmlReadFile(filepath_html, "UTF-8", HTML_PARSE_NOBLANKS |
			HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING | HTML_PARSE_NONET);
	root_element = xmlDocGetRootElement(doc);

	stations = 0;
	for (row = get_first_row(root_element); row; row = row->next)
	{
		callsign = row->children->children->children ?
			(char*)row->children->children->children->content : "";
		locator = row->children->next->children->children ?
			(char*)row->children->next->children->children->content : "";
		fprintf(exported_file, "%s;%s\n", callsign, locator);
		stations++;
	}
	*total_stations += stations;

	log_message("Processed page %s. %ld stations.", index, stations);
	xmlFreeDoc(doc); 
	if (!args->keep_htmls)
		remove(filepath_html);
	free(filepath_html);
	return (true);
}

int
main(int argc, char *argv[])
{
	struct args_s args;
	FILE *exported_file;
	double total_downloaded;
	long total_stations;

	LIBXML_TEST_VERSION

	if (!parse_arguments(argc, argv, &args))
		return (EXIT_FAILURE);

	/* Open exported CSV file for writing */
	if (!(exported_file = fopen(args.csv, "w")))
	{
		perror(args.csv);
		return (EXIT_FAILURE);
	}

	log_message("Initiating exporting process...");
	total_downloaded = 0;
	total_stations = 0;
	fputs("callsing;locator\n", exported_file);
	for (char i = args.first_page[0]; i <= args.last_page[0]; i++)
	{
		char j = (i == args.first_page[0] ? args.first_page[1] : 'A');
		while (j <= (i == args.last_page[0] ? args.last_page[1] : 'Z'))
		{
			if (!parse_table((char[]){i, j, '\0'}, exported_file, &args,
						&total_downloaded, &total_stations))
				break;
			j++;
		}
	}
	log_message("FINISHED. Total downloaded: %.2f MB. Total stations: %ld.",
			total_downloaded, total_stations);

	fclose(exported_file);
	xmlCleanupParser();
	return (EXIT_SUCCESS);
}
