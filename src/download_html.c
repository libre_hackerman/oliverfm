/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 * Copyright (C) 2021 Abdon Senén Cruces Pérez <JustMeGunter@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include <download_html.h>
#include <util.h>

/*
 * This function receives the index of a HTML page and downloads it returning
 * a heap allocated string with its path. It also increments the total
 * download counter with the size of the dowloaded data. Returns NULL on error.
 */

char *
download_html(const char *index, double *total_downloaded, struct args_s *args)
{
	char url[] = "https://www.dxmaps.com/callbook/infol.php?Locator=??%%";
	CURL *curl;
	FILE *f;
	char *filepath;
	double downloaded;

	filepath = strdup("radio_html_??.html");
	filepath[11] = index[0];
	filepath[12] = index[1];

	/* Check if destination file is already downloaded */
	if (args->keep_htmls && (f = fopen(filepath, "r")))
	{
		fclose(f);
		log_message("Already downloaded page %s.", index);
		return (filepath);
	}

	/* Open destination file for writing */
	if (!(f = fopen(filepath, "w")))
	{
		perror(filepath);
		free(filepath);
		return (NULL);
	}

	if (!(curl = curl_easy_init()))
	{
		fputs("Can't initialize libCurl\n", stderr);
		return (NULL);
	}

	url[50] = index[0];
	url[51] = index[1];
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, f);

	/* Download the file */
	take_it_easy(args->delay_download);
	log_message("Downloading page %s...", index);
	if (curl_easy_perform(curl) != 0)
	{
		fprintf(stderr, "Something went wrong downloading %s\n", url);
		fclose(f);
		remove(filepath);
		free(filepath);
		curl_easy_cleanup(curl);
		return (NULL);
	}
	fclose(f);
	downloaded = measure_filesize(filepath);
	log_message("Dowloaded size: %.2f MB.", downloaded);
	*total_downloaded += downloaded;

	curl_easy_cleanup(curl);
	return (filepath);
}
