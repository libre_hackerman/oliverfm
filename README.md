# Oliverfm

The purpose of this program is to scrap the [dxmaps.com](https://www.dxmaps.com/callbook/infol.php?Locator=A%25%25%25) page and export a CSV file with the *Callsign* and *Locator* fields of the stations registered on the site.

#### Use

```
$ oliverfm [-h] [-k] [-d DELAY] [-f PAGE] [-l PAGE] CSV
```
Just call the program passing the path to the CSV the program will create, and sit while it does its thing. It's *a lot* of data, plus some delays between downloads so you don't fry their servers, therefore it can take a while to fully complete.

__CLI options__:
- `-h`: display the help.
- `-k`: oliverfm downloads the HTML pages from dxmaps.com and parses the data inside them. If you want the program *not* to delete the downloaded HTMLs after finishing, set this option. Useful for the case when you want to regenerate the CSV but without having to redownload the HTMLs.
- `-d`: milliseconds to wait before downloading a HTML to be nice with the server: 200 ms by default.
- `-f`: Sets the first page to start parsing from: AA by default.
- `-l`: Sets the last page to parse from: ZZ by default.

#### Build (GNU/Linux)

##### Dependencies
- GCC/Clang
- GNU Make
- libcurl
- libxml2

##### Compilation
Just run `make`. If you want debug symbols, run `make debug`

##### Installation
You can directly run the compiled executable, but if you want to install it on your system, `make install` will do it. Notice that you can set the variable `PREFIX` (default to /usr/local) to your desire

##### Uninstall
`make uninstall` (Mind the `PREFIX` too)

#### FAQ
- **Why?**
Well, I'm not even sure what the fields the program is exporting are exactly, but a friend has a friend that does know and needed some program like this one, so we thought it would be a fun little program to do while alleviating someone else from a big and tedious task.
